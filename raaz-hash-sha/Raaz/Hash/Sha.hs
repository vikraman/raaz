{-# LANGUAGE DeriveDataTypeable         #-}
module Raaz.Hash.Sha
       ( SHA1(..)
       ) where

import Control.Applicative ((<$>), (<*>))
import Data.Bits(xor, (.|.))
import Data.Typeable(Typeable)
import Foreign.Ptr(plusPtr)
import Foreign.Storable(Storable(..))
import Test.QuickCheck(Arbitrary(..))

import Raaz.Types

-- | The SHA1 hash value.
data SHA1 = SHA1 {-# UNPACK #-} !Word32BE
                 {-# UNPACK #-} !Word32BE
                 {-# UNPACK #-} !Word32BE
                 {-# UNPACK #-} !Word32BE
                 {-# UNPACK #-} !Word32BE deriving (Show, Typeable)

-- | Timing independent equality testing.
instance Eq SHA1 where
  (==) (SHA1 g0 g1 g2 g3 g4) (SHA1 h0 h1 h2 h3 h4) =   xor g0 h0
                                                   .|. xor g1 h1
                                                   .|. xor g2 h2
                                                   .|. xor g3 h3
                                                   .|. xor g4 h4
                                                   == 0


instance Storable SHA1 where
  sizeOf    _ = 5 * sizeOf (undefined :: Word32BE)
  alignment _ = alignment  (undefined :: Word32BE)
  peekByteOff ptr pos = do
    h0 <- peekByteOff ptr pos
    h1 <- peekByteOff ptr (pos + 4)
    h2 <- peekByteOff ptr (pos + 8)
    h3 <- peekByteOff ptr (pos + 12)
    h4 <- peekByteOff ptr (pos + 16)
    return $ SHA1 h0 h1 h2 h3 h4
  pokeByteOff ptr pos (SHA1 h0 h1 h2 h3 h4) =  pokeByteOff ptr pos        h0
                                            >> pokeByteOff ptr (pos + 4)  h1
                                            >> pokeByteOff ptr (pos + 8)  h2
                                            >> pokeByteOff ptr (pos + 12) h3
                                            >> pokeByteOff ptr (pos + 16) h4

instance CryptoStore SHA1 where
  load cptr = do h0 <- load cptr
                 h1 <- load $ plusPtr cptr 4
                 h2 <- load $ plusPtr cptr 8
                 h3 <- load $ plusPtr cptr 12
                 h4 <- load $ plusPtr cptr 16
                 return $ SHA1 h0 h1 h2 h3 h4


  store cptr (SHA1 h0 h1 h2 h3 h4) =  store cptr                h0
                                   >> store (cptr `plusPtr` 4)  h1
                                   >> store (cptr `plusPtr` 8)  h2
                                   >> store (cptr `plusPtr` 12) h3
                                   >> store (cptr `plusPtr` 16) h4

instance Arbitrary SHA1 where
  arbitrary = SHA1 <$> arbitrary   -- h0
                   <*> arbitrary   -- h1
                   <*> arbitrary   -- h2
                   <*> arbitrary   -- h3
                   <*> arbitrary   -- h4
