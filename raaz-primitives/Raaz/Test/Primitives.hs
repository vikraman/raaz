{-
This Module contains some test cases for classes in Primitives.
-}

module Raaz.Test.Primitives 
       ( testPadLengthVsPadding
       , testLengthDivisibility
       ) where

import qualified Data.ByteString as B
import Data.Word
import Test.Framework(Test)
import Test.Framework.Providers.QuickCheck2(testProperty)

import Raaz.Types
import Raaz.Primitives

prop_padLengthVsPadding :: Compressor c => c -> BITS Word64 ->  Bool
prop_padLengthVsPadding c w = padLength c w == 
                              fromIntegral (B.length $ padding c w)

prop_LengthDivisibility :: Compressor c => c -> BITS Word64 -> Bool
prop_LengthDivisibility c w = len `rem` blockSize c == 0
    where len = padLength c w + cryptoCoerce w
          
-- | For a compressor, this test checks whether the padding length
-- computed using the function @`padLength`@ is equal to the length of
-- the bytestring returned by the function @`padding`@.
testPadLengthVsPadding :: Compressor c => c -> Test
testPadLengthVsPadding c =  testProperty "padLength == Length . padding"
                                     $ prop_padLengthVsPadding c

-- | For a compressor, this test checks whether the sum of the message
-- length and padding length is a multiple of the block length.
testLengthDivisibility :: Compressor c => c -> Test
testLengthDivisibility c = testProperty "padding + message length vs block length"
                           $ prop_LengthDivisibility c
