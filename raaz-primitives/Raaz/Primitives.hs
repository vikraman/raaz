{-|

Generic cryptographic algorithms.

-}

{-# LANGUAGE TypeFamilies                #-}
{-# LANGUAGE MultiParamTypeClasses       #-}
{-# LANGUAGE GeneralizedNewtypeDeriving  #-}
module Raaz.Primitives
       ( BlockPrimitive(..)
       , BLOCKS, blocksOf
       , Compressor(..)
       , compressByteString
       , compressLazyByteString
       ) where

import Data.Word
import qualified Data.ByteString as B
import qualified Data.ByteString.Lazy as L
import Data.ByteString.Internal (unsafeCreate)
import Foreign.Ptr(castPtr)
import Foreign.Marshal.Alloc(allocaBytes)

import Raaz.Types
import Raaz.Util.ByteString(fillUp)



-- | Abstraction that captures crypto primitives that work one block
-- at a time. Examples are block ciphers, Merkle-Damgård hashes etc.
class BlockPrimitive p where
  blockSize :: p -> BYTES Int -- ^ Block size

-- | Type safe message length in units of blocks of the primitive.
newtype BLOCKS p = BLOCKS Int
                 deriving (Show, Eq, Ord, Enum, Real, Num, Integral)

instance ( BlockPrimitive p
         , Num by
         ) => CryptoCoerce (BLOCKS p) (BYTES by) where
  cryptoCoerce b@(BLOCKS n) = fromIntegral $ blockSize (prim b) * 
                                           (fromIntegral n)
         where prim :: BLOCKS p -> p
               prim _ = undefined
  {-# INLINE cryptoCoerce #-}


instance ( BlockPrimitive p
         , Num bits
         ) => CryptoCoerce (BLOCKS p) (BITS bits) where
  cryptoCoerce b@(BLOCKS n) = fromIntegral $ 8 * blockSize (prim b) * 
                                           (fromIntegral n)
         where prim :: BLOCKS p -> p
               prim _ = undefined
  {-# INLINE cryptoCoerce #-}

-- | BEWARE: There can be rounding errors if the number of bytes is
-- not a multiple of block length.
instance ( BlockPrimitive p
         , Integral by
         ) => CryptoCoerce (BYTES by) (BLOCKS p) where
  cryptoCoerce bytes = result
         where prim :: BLOCKS p -> p
               prim _ = undefined
               result = BLOCKS (fromIntegral m)
               m      = fromIntegral bytes `quot` blockSize (prim result)
  {-# INLINE cryptoCoerce #-}

-- | The expression @n `blocksOf` p@ specifies the message lengths in
-- units of the block length of the primitive @p@. This expression is
-- sometimes required to make the type checker happy.
blocksOf :: BlockPrimitive p =>  Int -> p -> BLOCKS p
blocksOf n _ = BLOCKS n

-- | A compressor is a crypto primitive that shrinks an arbitrary
-- sized string to a fixed size string. A message is usually padded
-- using some strategy. The obvious need for padding is to handle
-- messages that do not have a length that is a multiple of the block
-- length. Besides the security of certain primitives like the
-- Merkle-Damgård hashes depend on the padding.
--
-- [Note on message lengths:] All message lengths are in bits as
-- hashing schemes like the Merkle-Damgård and HAIFA use the number of
-- bits for strengthening etc. Besides it is easy to compute the
-- number of bytes (if required) from this parameter.


class BlockPrimitive c => Compressor c where
  -- | The compress context.
  type Cxt c   :: *

  -- | Compresses blocks of data. This function is not meant to handle
  -- partially blocks.
  compress :: c               -- ^ The compressor.
           -> Cxt c           -- ^ The current state.
           -> BITS Word64     -- ^ Number of bits processed so far
                              -- excluding the data available at the
                              -- crypto buffer.
           -> CryptoPtr       -- ^ Data buffer containing the next block.
                              -- Implementations should ensure that the data
                              -- is undisturbed.
           -> BLOCKS c        -- ^ Number of blocks at the above location.
           -> IO (Cxt c)

  -- | Although the compressor works one block at a time, handling the
  -- last block of data requires additional blocks mainly to handle
  -- padding. This variable gives the number of additional blocks
  -- required to handle this. In a hashing algorithm like @SHA1@ this
  -- is 1 (this the the additional block and does not count the
  -- partially filled block if there is any).
  maxAdditionalBlocks :: c -> BLOCKS c

  -- | The length of the padding message for a given length.
  padLength :: c            -- ^ The compressor
            -> BITS  Word64 -- ^ Total length of the message.
            -> BYTES Int

  -- | This function gives the padding to add to the message.
  padding :: c           -- ^ The compressor
          -> BITS Word64 -- ^ Total length of the message.
          -> B.ByteString

  padding c len = unsafeCreate (fromIntegral $ padLength c len)
                               (unsafePadIt c len 0 . castPtr)

  -- | This is the unsafe version of the padding where it is assumed
  -- that the data buffer has enough space to accomodate the padding
  -- data.
  unsafePadIt :: c           -- ^ The compressor
              -> BITS Word64 -- ^ The length of the message in bits
              -> BYTES Int   -- ^ Position to start padding.
              -> CryptoPtr   -- ^ The buffer to put the padding
              -> IO ()

-- | Compress a strict bytestring.
compressByteString :: Compressor c
                   => c             -- ^ The compressor
                   -> Cxt c         -- ^ The starting context
                   -> B.ByteString  -- ^ The input bytestring
                   -> IO (Cxt c)
compressByteString c cxt bs = compressChunks c cxt [bs]

-- | Compress a lazy bytestring.
compressLazyByteString :: Compressor c
                       => c            -- ^ The compressor
                       -> Cxt c        -- ^ the starting context
                       -> L.ByteString -- ^ the input bytestring
                       -> IO (Cxt c)
compressLazyByteString c cxt = compressChunks c cxt . L.toChunks

compressChunks :: ( BlockPrimitive c
                  , Compressor c
                  )
               => c
               -> Cxt c
               -> [B.ByteString]
               -> IO (Cxt c)

compressChunks c context chunks =
  allocaBytes (fromIntegral sz) (go context (0 :: BITS  Word64)
                                            (0 :: BYTES Int)
                                            chunks
                                )
    where sz      = cryptoCoerce (maxAdditionalBlocks c + 1) :: BYTES Int
          go cxt tsize r [] cptr = do
                 unsafePadIt c l r cptr
                 compress c cxt l cptr blks
             where l      = tsize + cryptoCoerce r
                   pl     = padLength c l
                   blks   = cryptoCoerce (r + pl)
          go cxt tsize r (b:bs) cptr = do
                 erem <- fillUp (cryptoCoerce (1 `blocksOf` c)) cptr r b
                 case erem of
                      Left  r' -> go cxt tsize r' bs cptr
                      Right b' -> do cxt' <- compress c cxt tsize cptr 1
                                     go cxt' tsize' 0 (b':bs) cptr
             where tsize' = tsize + cryptoCoerce (1 `blocksOf` c)
