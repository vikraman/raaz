{-|

A cryptographic hash function abstraction.

-}

{-# LANGUAGE TypeFamilies      #-}
{-# LANGUAGE FlexibleContexts  #-}
module Raaz.Hash
       ( CryptoHash(..)
       ) where

import Data.Word
import Foreign.Ptr

import Raaz.Types
import Raaz.Primitives

-- | The class that captures a Hash function. The associated type
-- @Hash h@ captures the actual Hash value. Minimum complete
-- definition includes defining @`startHashCxt`, @`finaliseHash`@.
-- However implementations might want giving efficient implementations
-- of the other functions as well.
--
-- [Warning:] While defining the @'Eq'@ instance of @'Hash' h@, make
-- sure that the @==@ operator takes time independent of the input.
-- This is to avoid timing based side-channel attacks. In particular,
-- /do not/ take the lazy option of deriving the @'Eq'@ instance.
class ( Compressor  h
      , Eq          (Hash h)
      , CryptoStore (Hash h)
      ) => CryptoHash h where

  -- | The hash value.
  type Hash h    :: *

  -- | Alloc a new context for use.
  startHashCxt   :: h -> Cxt h

  -- | Finalise the context to a hash value.
  finaliseHash   :: h -> Cxt h -> Hash h

  -- | Often we want to append the hash of the message at the end of a
  -- message packet. This unsafe function is meant for such an
  -- operation. As the name suggests no checks are (or can be) done on
  -- the size of the message buffer. Implementations can expect that
  -- there is enough space at the end of the message bytes to add the
  -- padding and subsequently store the hash's output. Perhaps the
  -- following figure helps visualise the length requirement of the
  -- crypto buffer: The buffer starts out with the following in the
  -- crypto buffer.
  --
  -- > +---------+------------------------------------------------+
  -- > | Message | free space to hold the maximum of padding and  |
  -- > |         |              the final hash value              |
  -- > +---------+------------------------------------------------+
  --
  -- After a call of @`unsafeHashAndAppend`@ the message buffer should
  -- look like
  --
  -- > +---------+--------------+---------------------------------+
  -- > | Message | hash of the  |  vacant space if any (contents  |
  -- > |         |   message    |           undefined)            |
  -- > +---------+--------------+---------------------------------+
  -- >
  --
  -- A default definition of this function is given, although it is
  -- expected that actual implementations would have a more efficient
  -- implementation.
  unsafeHashAndAppend :: h              -- ^ the hash algorithm
                      -> Cxt h          -- ^ the context
                      -> BITS Word64    -- ^ the number of bits of
                                        -- message hashed so far (not
                                        -- including the data in in
                                        -- the buffer).
                      -> CryptoPtr      -- ^ the message buffer
                      -> BYTES Int      -- ^ length of message in
                                        -- bytes.
                      -> IO ()
  unsafeHashAndAppend h cxt mlen cptr len = do
          unsafePadIt h tL len cptr
          cxt' <- compress h cxt mlen cptr blks
          let hsh = finaliseHash h cxt'
              in store cptr' hsh
    where tL         = mlen + cryptoCoerce len
          pL         = cryptoCoerce (padLength h tL) :: BYTES Int
          blks       = cryptoCoerce (len + pL)
          cptr'      = cptr `plusPtr` fromIntegral len


  -- | Use this method if you want to overwrite the message with its
  -- hash. Implementations can expect that there is enough space at to
  -- hold the maximul of the message bytes, padding and final hash value.
  -- Here is a diagram depicting the message.
  --
  -- > +---------+------------------------------------------------+
  -- > | Message |additional free space beyond the message lenght |
  -- > |         | big enough to hold the maximum of padding and  |
  -- > |         |the final hash value if the message is shorter. |
  -- > +---------+------------------------------------------------+
  --
  -- After a call of @`unsafeHashOverwrite`@ the message buffer should
  -- look like
  --
  -- > +--------------+-------------------------------------------+
  -- > | hash of the  | vacant space if any (contents undefined)  |
  -- > |   message    |                                           |
  -- > +--------------+-------------------------------------------+
  -- >
  --
  -- A default definition of this function is given, although it is
  -- expected that actual implementations would have a more efficient
  -- implementation.

  unsafeHashOverwrite :: h              -- ^ the hash algorithm
                      -> Cxt h          -- ^ the context
                      -> BITS Word64    -- ^ the number of bits of message
                                        -- hashed so far.
                      -> CryptoPtr      -- ^ the message buffer
                      -> BYTES Int      -- ^ length of the message
                      -> IO ()
  unsafeHashOverwrite h cxt mlen cptr len = do
          unsafePadIt h tL len cptr
          cxt' <- compress h cxt mlen cptr blks
          let hsh = finaliseHash h cxt'
              in store cptr hsh
    where tL         = mlen + cryptoCoerce len
          pL         = cryptoCoerce (padLength h tL) :: BYTES Int
          blks       = cryptoCoerce (len + pL)

